# Python Clean Architecture example.

The problem is taken from [The Advent of Code(AoC) 2018, day 3](https://adventofcode.com/2018/day/3).

The purpose of this project is to demonstrate the use of certain patterns and best practices in python.

##### Doubts
* The UnitOfWork where must by handle.
* src/domain/office_wall_facade.py:28 uses the repository directly in order to avoid loading the object.