#!/usr/bin/env python

from cleo import Application

from console.src.commands import OverlappedClaimsCounterCommand, IdsOfNonOverlappedClaimsCommand

application = Application()
application.add(IdsOfNonOverlappedClaimsCommand())
application.add(OverlappedClaimsCounterCommand())


def main():
    application.run()


if __name__ == '__main__':
    main()
