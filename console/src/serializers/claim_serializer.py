import re

from core.src.domain.builders import ClaimBuilder
from core.src.domain.models import Claim, ClaimID


class ClaimLineSerializer:
    line_regex = re.compile(
        r"^#(?P<identifier>\d+) @ (?P<point_x>\d+),(?P<point_y>\d+): (?P<width>\d+)x(?P<height>\d+)$"
    )

    def __init__(self, line):
        self._line = line

    def serialize(self) -> Claim:
        matched_line = self.line_regex.match(self._line)
        builder = ClaimBuilder(
            identifier=ClaimID(int(matched_line.group('identifier'))),
            x=int(matched_line.group('point_x')),
            y=int(matched_line.group('point_y')),
            width=int(matched_line.group('width')),
            height=int(matched_line.group('height'))
        )
        return builder()
