from dataclasses import dataclass
from pathlib import Path
from typing import ClassVar

from cleo import Command

from console.src.serializers.claim_serializer import ClaimLineSerializer
from core.src.application.application import Application
from core.src.application.create_claim import CreateClaim
from core.src.application.ids_of_non_overlapped_claims import IdsOfNonOverlappedClaims
from core.src.application.overlapped_claims_counter import OverlappedClaimsCount
from core.src.domain.models import Claim
from core.src.infrastructure.persistence.memory.unit_of_work import MemoryUnitOfWorkManager
from core.src.infrastructure.persistence.unit_of_work import UnitOfWorkManager


@dataclass
class ClaimSolverCommand(Command):

    def __init__(self, name=None):
        super().__init__(name)
        self.unit_of_work_manager: UnitOfWorkManager = MemoryUnitOfWorkManager()

    @property
    def application_solver(self) -> ClassVar[Application]:
        raise NotImplementedError

    @staticmethod
    def _parse_line(line: str) -> Claim:
        serializer = ClaimLineSerializer(line)
        claim = serializer.serialize()
        return claim

    def parse_input(self, f):
        lines = (x.strip() for x in f.readlines())
        return map(self._parse_line, lines)

    def handle(self) -> int:
        input_path = self.argument('path')
        with Path(input_path).open() as f:
            claims = self.parse_input(f)
            with self.unit_of_work_manager.start() as unit_of_work:
                create_claim = CreateClaim(unit_of_work=unit_of_work)
                for claim in claims:
                    create_claim(claim)
                application_solver = self.application_solver(unit_of_work)
                solution = application_solver()
                unit_of_work.commit()
                self.line(str(solution))


class OverlappedClaimsCounterCommand(ClaimSolverCommand):
    """
    Number of overlapped claims

    occ
        {path : Path of claim ledger}
    """

    @property
    def application_solver(self) -> ClassVar[Application]:
        return OverlappedClaimsCount


class IdsOfNonOverlappedClaimsCommand(ClaimSolverCommand):
    """
    Ids of non overlapped claims

    noc
        {path : Path of claim ledger}
    """

    @property
    def application_solver(self) -> ClassVar[Application]:
        return IdsOfNonOverlappedClaims
