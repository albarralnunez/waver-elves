import pytest

from core.src.domain.models import Area, Position, ClaimID, Claim, FabricPosition


@pytest.mark.parametrize("test_input,expected", [
    (Area(Position(0, 0), 2, 2),
     [Position(0, 0), Position(0, 1), Position(1, 0), Position(1, 1)])
])
def test_area_get_all_tails(test_input, expected):
    result = test_input.get_all_tails()
    assert list(result) == expected


@pytest.mark.parametrize("test_input,expected", [
    ({"identifier": ClaimID(10), "area": Area(Position(0, 0), 2, 2)},
     Claim(identifier=ClaimID(10), area=Area(Position(0, 0), 2, 2)))
])
def test_claim_create(test_input, expected):
    result = Claim.create(**test_input)
    assert result == expected


@pytest.mark.parametrize("test_input,expected", [
    (Claim(identifier=ClaimID(1), area=Area(Position(0, 0), 2, 2)),
     [Position(0, 0), Position(0, 1), Position(1, 0), Position(1, 1)])
])
def test_claim_get_claim_positions(test_input, expected):
    result = test_input.get_claim_positions()
    assert list(result) == expected


@pytest.mark.parametrize("test_input,expected", [
    ({"position": Position(1, 1)},
     FabricPosition(
         position=Position(1, 1),
         claims_ids=set()
     ))
])
def test_fabric_position_create(test_input, expected):
    result = FabricPosition.create(**test_input)
    assert result == expected


@pytest.mark.parametrize("test_input,expected", [
    (FabricPosition(position=Position(1, 1), claims_ids=set()),
     FabricPosition(
         position=Position(1, 1), claims_ids={ClaimID(1)}))
])
def test_fabric_position_add(test_input, expected):
    test_input.add(ClaimID(1))
    assert test_input == expected
