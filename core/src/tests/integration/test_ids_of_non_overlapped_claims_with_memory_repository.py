import pytest

from core.src.application.create_claim import CreateClaim
from core.src.application.ids_of_non_overlapped_claims import IdsOfNonOverlappedClaims
from core.src.domain.models import ClaimID, Claim, Area, Position
from core.src.infrastructure.persistence.memory.unit_of_work import MemoryUnitOfWorkManager


@pytest.mark.parametrize("claims,expected", [
    (
            [
                Claim(ClaimID(1), area=Area(Position(0, 0), 2, 2)),
                Claim(ClaimID(2), area=Area(Position(10, 10), 2, 2)),
                Claim(ClaimID(3), area=Area(Position(11, 11), 2, 2)),
            ],
            {ClaimID(1)}
    )
])
def test_ids_of_non_overlapped_claims_with_memory_repository(claims, expected):
    unit_of_work_manager = MemoryUnitOfWorkManager()
    with unit_of_work_manager.start() as unit_of_work:
        create_claim_app = CreateClaim(unit_of_work)
        for claim in claims:
            create_claim_app(claim)
        solver_app = IdsOfNonOverlappedClaims(unit_of_work)
        result = solver_app()
    assert result == expected
