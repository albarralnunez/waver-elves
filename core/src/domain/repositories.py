from typing import Iterable, Set

from core.src.domain.models import ClaimID, FabricPosition, FabricPositionClaim, Position


class OnlyClaimedOnesRepository:

    def get_all(self) -> Set[ClaimID]:
        raise NotImplementedError

    def exists(self, non_overlapped_claim_id: ClaimID) -> bool:
        raise NotImplementedError

    def save(self, non_overlapped_claim_id: ClaimID):
        raise NotImplementedError

    def remove(self, non_overlapped_claim_id: ClaimID):
        raise NotImplementedError


class FabricPositionRepository:

    def save(self, fabric_position: FabricPosition):
        raise NotImplementedError

    def add_claim(self, fabric_claim: FabricPositionClaim):
        pass

    def exists_position(self, position: Position) -> bool:
        raise NotImplementedError

    def get_claims_at_position(self, position: Position) -> Iterable[ClaimID]:
        raise NotImplementedError

    def get_number_of_overlapped_claims(self) -> int:
        raise NotImplementedError
