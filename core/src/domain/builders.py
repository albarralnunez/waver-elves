from dataclasses import dataclass

from core.src.domain.models import ClaimID, Coordinates, Distance, Claim, Area, Position


@dataclass
class ClaimBuilder:
    identifier: ClaimID
    x: Coordinates
    y: Coordinates
    width: Distance
    height: Distance

    def __call__(self):
        return Claim.create(
            identifier=self.identifier,
            area=Area(
                starting_point=Position(
                    x=self.x,
                    y=self.y
                ),
                width=self.width,
                height=self.height
            )
        )
