from dataclasses import dataclass
from typing import Iterable, Set

Coordinates = int
Distance = int


@dataclass(frozen=True)
class Position:
    x: Coordinates
    y: Coordinates


@dataclass(frozen=True)
class Area:
    starting_point: Position
    width: Distance
    height: Distance

    def get_all_tails(self):
        x_positions_end = self.starting_point.x + self.width
        y_positions_end = self.starting_point.y + self.height
        x_positions = range(self.starting_point.x, x_positions_end)
        y_positions = range(self.starting_point.y, y_positions_end)
        return (Position(x_position, y_position) for x_position in x_positions for y_position in
                y_positions)


@dataclass(frozen=True)
class ClaimID:
    value: int


@dataclass(frozen=True)
class Claim:
    identifier: ClaimID
    area: Area

    @classmethod
    def create(cls, identifier: ClaimID, area: Area):
        return cls(identifier, area)

    def get_claim_positions(self) -> Iterable[Position]:
        return self.area.get_all_tails()


@dataclass
class FabricPositionClaim:
    position: Position
    claim_id: ClaimID


@dataclass
class FabricPosition:
    position: Position
    claims_ids: Set[ClaimID]

    @classmethod
    def create(cls, position: Position):
        return cls(
            position=position,
            claims_ids=set()
        )

    def add(self, claim_id):
        self.claims_ids.add(claim_id)
