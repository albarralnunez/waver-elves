from dataclasses import dataclass

from core.src.domain.models import Claim, FabricPositionClaim, FabricPosition
from core.src.domain.repositories import OnlyClaimedOnesRepository, FabricPositionRepository


@dataclass(frozen=True)
class FabricFacade:
    only_claimed_ones_repository: OnlyClaimedOnesRepository
    fabric_position_repository: FabricPositionRepository

    def _remove_from_claimed_ones(self, position):
        for claim_id_to_remove in self.fabric_position_repository.get_claims_at_position(position):
            if self.only_claimed_ones_repository.exists(claim_id_to_remove):
                self.only_claimed_ones_repository.remove(claim_id_to_remove)

    def make_claim(self, claim: Claim):
        self.only_claimed_ones_repository.save(claim.identifier)
        for position in claim.get_claim_positions():
            fabric_position_claim = FabricPositionClaim(position, claim.identifier)
            if self.fabric_position_repository.exists_position(fabric_position_claim.position):
                self.fabric_position_repository.add_claim(fabric_position_claim)
                self._remove_from_claimed_ones(position)
            else:
                fabric_position = FabricPosition.create(fabric_position_claim.position)
                fabric_position.add(fabric_position_claim.claim_id)
                self.fabric_position_repository.save(fabric_position)
