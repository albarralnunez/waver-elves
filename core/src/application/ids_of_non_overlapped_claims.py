from dataclasses import dataclass
from typing import Set

from core.src.application.application import Application
from core.src.domain.models import ClaimID
from core.src.infrastructure.persistence.unit_of_work import UnitOfWork


@dataclass
class IdsOfNonOverlappedClaims(Application):
    unit_of_work: UnitOfWork

    def __call__(self) -> Set[ClaimID]:
        return self.unit_of_work.only_claimed_ones.get_all()
