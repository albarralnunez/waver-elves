from dataclasses import dataclass

from core.src.application.application import Application
from core.src.infrastructure.persistence.unit_of_work import UnitOfWork


@dataclass
class OverlappedClaimsCount(Application):
    unit_of_work: UnitOfWork

    def __call__(self) -> int:
        return self.unit_of_work.fabric_positions.get_number_of_overlapped_claims()
