from dataclasses import dataclass

from core.src.application.application import Application
from core.src.domain.fabric_facade import FabricFacade
from core.src.infrastructure.persistence.unit_of_work import UnitOfWork


@dataclass
class CreateClaim(Application):
    unit_of_work: UnitOfWork

    def __call__(self, claim):
        fabric = FabricFacade(
            only_claimed_ones_repository=self.unit_of_work.only_claimed_ones,
            fabric_position_repository=self.unit_of_work.fabric_positions
        )
        fabric.make_claim(claim)
        self.unit_of_work.commit()
