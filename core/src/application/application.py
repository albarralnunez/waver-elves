from dataclasses import dataclass


@dataclass
class Application:

    def __call__(self, *args, **kwargs):
        raise NotImplementedError
