from core.src.domain.repositories import OnlyClaimedOnesRepository, FabricPositionRepository


class UnitOfWork:
    """The unit of work captures the idea of a set of things that
       need to happen together.

       Usually, in a relational database,
       one unit of work == one database transaction."""

    def __init__(self, session_factory=None):
        self.session_factory = session_factory

    def __enter__(self) -> 'UnitOfWork':
        raise NotImplementedError

    def __exit__(self, type, value, traceback):
        raise NotImplementedError

    def commit(self):
        raise NotImplementedError

    def rollback(self):
        raise NotImplementedError

    @property
    def only_claimed_ones(self) -> OnlyClaimedOnesRepository:
        raise NotImplementedError

    @property
    def fabric_positions(self) -> FabricPositionRepository:
        raise NotImplementedError


class UnitOfWorkManager:
    """The Unit of work manager returns a new unit of work.
       Our UOW is backed by a sql alchemy session whose
       lifetime can be scoped to a web request, or a
       long-lived background job."""

    def __init__(self, session_maker=None):
        self.session_maker = session_maker

    def start(self) -> UnitOfWork:
        raise NotImplementedError
