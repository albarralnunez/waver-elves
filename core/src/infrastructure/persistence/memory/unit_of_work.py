from core.src.domain.repositories import OnlyClaimedOnesRepository, FabricPositionRepository
from core.src.infrastructure.persistence.memory.repositories.fabric_position_repository import \
    FabricPositionRepositoryMemory
from core.src.infrastructure.persistence.memory.repositories.only_claimed_ones_repository import \
    OnlyClaimedOnesRepositoryMemory
from core.src.infrastructure.persistence.unit_of_work import UnitOfWorkManager, UnitOfWork


class MemoryUnitOfWork(UnitOfWork):
    """The unit of work captures the idea of a set of things that
       need to happen together.

       Usually, in a relational database,
       one unit of work == one database transaction."""

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass

    def commit(self):
        pass

    def rollback(self):
        pass

    @property
    def only_claimed_ones(self) -> OnlyClaimedOnesRepository:
        return OnlyClaimedOnesRepositoryMemory()

    @property
    def fabric_positions(self) -> FabricPositionRepository:
        return FabricPositionRepositoryMemory()


class MemoryUnitOfWorkManager(UnitOfWorkManager):
    """The Unit of work manager returns a new unit of work.
       Our UOW is backed by a sql alchemy session whose
       lifetime can be scoped to a web request, or a
       long-lived background job."""

    def start(self):
        return MemoryUnitOfWork()
