from typing import Iterable, Set

from core.src.domain.models import ClaimID
from core.src.domain.repositories import OnlyClaimedOnesRepository


class OnlyClaimedOnesRepositoryMemory(OnlyClaimedOnesRepository):

    values: Set[ClaimID] = set()

    def get_all(self) -> Iterable[ClaimID]:
        return self.values

    def exists(self, non_overlapped_claim_id: ClaimID) -> bool:
        return non_overlapped_claim_id in self.values

    def save(self, non_overlapped_claim_id: ClaimID):
        self.values.add(non_overlapped_claim_id)

    def remove(self, non_overlapped_claim_id: ClaimID):
        self.values.remove(non_overlapped_claim_id)
