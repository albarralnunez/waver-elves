from typing import Dict, Set

from core.src.domain.models import Position, ClaimID, FabricPosition, FabricPositionClaim
from core.src.domain.repositories import FabricPositionRepository


class FabricPositionRepositoryMemory(FabricPositionRepository):

    values: Dict[Position, Set[ClaimID]] = dict()

    def save(self, fabric_position: FabricPosition):
        self.values[fabric_position.position] = set(fabric_position.claims_ids)

    def add_claim(self, fabric_claim: FabricPositionClaim):
        self.values[fabric_claim.position].add(fabric_claim.claim_id)

    def exists_position(self, position: Position):
        return position in self.values

    def get_claims_at_position(self, position: Position):
        return self.values[position]

    def get_number_of_overlapped_claims(self):
        return len(list(filter(lambda x: len(x) > 1, self.values.values())))
